<?php include'header.php'; ?>

<!--    [ Strat Section Title Area]-->
<!--    [Finish Section Title Area]-->
<section id="summery" class="body-part">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="page-title text-center">
                    <h3>Control Panel Summary</h3>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-3">
                <div class="bl-user-info text-center">
                    <div class="users-icon-bar glob">
                        <i class="icofont icofont-users-alt-2"></i>
                    </div>
                    <div class="users-ifo-bar">
                        <p>Total Subscriber</p>
                        <h4>2066</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="bl-user-info text-center">
                    <div class="users-icon-bar face social-ng">
                        <i class="icofont icofont-users-alt-2"></i>
                    </div>
                    <div class="users-ifo-bar social-ng">
                        <p>Total Subscriber</p>
                        <h4>2066</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="bl-user-info text-center">
                    <div class="users-icon-bar glob">
                        <i class="icofont icofont-mail-box"></i>
                    </div>
                    <div class="users-ifo-bar">
                        <p>Total Message</p>
                        <h4>2066</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="bl-user-info text-center">
                    <div class="users-icon-bar face social-ng">
                        <i class="icofont icofont-mail-box"></i>
                    </div>
                    <div class="users-ifo-bar social-ng">
                        <p>Total Message</p>
                        <h4>2066</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-3">
                <div class="bl-user-info text-center">
                    <div class="users-icon-bar glob">
                        <i class="icofont icofont-full-sunny"></i>
                    </div>
                    <div class="users-ifo-bar">
                        <p>Daily Subscriber</p>
                        <h4>2066</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="bl-user-info text-center">
                    <div class="users-icon-bar face social-ng">
                        <i class="icofont icofont-speech-comments"></i>
                    </div>
                    <div class="users-ifo-bar social-ng">
                        <p>Daily Message</p>
                        <h4>2066</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="bl-user-info text-center">
                    <div class="users-icon-bar glob">
                        <i class="icofont icofont-users-social"></i>
                    </div>
                    <div class="users-ifo-bar">
                        <p>Today's User</p>
                        <h4>2066</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="bl-user-info text-center">
                    <div class="users-icon-bar face social-ng">
                        <i class="icofont icofont-users-alt-1"></i>
                    </div>
                    <div class="users-ifo-bar social-ng">
                        <p>Today's User</p>
                        <h4>2066</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-3">
                <div class="bl-user-info text-center">
                    <div class="users-icon-bar glob">
                        <i class="icofont icofont-user-male"></i>
                    </div>
                    <div class="users-ifo-bar">
                        <p>Male</p>
                        <h4>2066</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="bl-user-info text-center">
                    <div class="users-icon-bar face social-ng">
                        <i class="icofont icofont-user-female"></i>
                    </div>
                    <div class="users-ifo-bar social-ng">
                        <p>Female</p>
                        <h4>2066</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="bl-user-info text-center">
                    <div class="users-icon-bar glob">
                        <i class="icofont icofont-dice-alt"></i>
                    </div>
                    <div class="users-ifo-bar">
                        <p>Denied</p>
                        <h4>2066</h4>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<!--    [ Strat Section Area]-->
<!--    [Finish Section Area]-->
<?php include'footer.php'; ?>
