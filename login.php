<?php include 'header.php';?>
<style>
    header {
        display: none;
    }

    .company-logo img {
        border: 1px solid #F97E32;
        padding: 20px 0;
    }

    .log-title h4 {
        margin: 30px 0;
        font-size: 20px;
        font-weight: 300;
        color: #F97E32;
    }

    .log-table {
        margin-bottom: 110px;
    }

</style>
<!--    [ Strat Section Area]-->
<section id="add-admin" class="body-part">
    <div class="container">
        <div class="log-table">
            <div class="log-table-cell">

                <div class="row justify-content-center">
                    <div class="col-lg-6">

                        <div class="log-form text-center">
                            <div class="brand">
                                <a href="index.php"><img src="assets/img/logo.png" alt=""></a>
                            </div>
                            <div class="log-title">
                                <h4>Login Banglalink Control Panel</h4>
                            </div>
                            <form action="">
                                <div class="input-group">
                                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                    <input type="email" class="form-control" placeholder="Email">
                                </div>
                                <div class="input-group">
                                    <i class="fa fa-key" aria-hidden="true"></i>
                                    <input type="password" class="form-control" placeholder="Password">
                                </div>
                                <div class="submit-btn">
                                    <button>Login</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Area]-->
<?php include 'footer.php';?>
